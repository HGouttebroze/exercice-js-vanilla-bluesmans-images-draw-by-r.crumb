"use strict";

/*************************************************************************************************/
/* ***************************************** FONCTIONS ***************************************** */
/*************************************************************************************************/
function compteur() {
  let selectImages = document.querySelectorAll(".photo-list li.selected");
  //console.log("test");
  document.querySelector("#total em").textContent = selectImages.length;
}

function selectOne() {
  this.classList.toggle("selected");
  compteur();
}

function selectAll() {
  for (let image of images) {
    image.classList.add("selected");
  }
  //console.log("test");

  compteur();
}

function deselectAll() {
  for (let image of images) {
    image.classList.remove("selected");
  }
  //console.log("test");
  compteur();
}

function toggleSelect() {
  console.log(this.dataset.mode);
  if (this.dataset.mode == "select") {
    for (let image of images) {
      image.classList.add("selected");
    }
    this.dataset.mode = "deselected";
    this.textContent = "Deselectionner tout";
  } else {
    for (let image of images) {
      image.classList.remove("selected");
    }
    this.dataset.mode = "select";
    this.textContent = "Selectionner tout";
  }
}

// /*************************************************************************************************/
// /* ************************************** CODE PRINCIPAL *************************************** */
// /*************************************************************************************************/

const images = document.querySelectorAll(".photo-list li");
// il faut parcourir un tableau
// avec un for classique, bien si on a besoin d'un index, et c'est la + performante !!!

// for (let i = 0; i < onClickImg.length; i++) {
//   onClickImg[i].addEventListener("click", selectOne);
// }

// avec un for...of
// for (let img of onClickImg) {
//   img.addEventListener("click", selectOne);
// }

// avec un forEach (en fonction fléchée) (on part du tableau et on fait référence à 1 fonction, qui récupere la ligne courante)
// on pt mettre 1 index: onClickImg.forEach((img, index) => {

images.forEach((image) => image.addEventListener("click", selectOne)); // Installation d'un gestionnaire d'évènement clic sur toutes les balises <li>

document.querySelector("#selectAll").addEventListener("click", selectAll);

document.querySelector("#deselectAll").addEventListener("click", deselectAll);

document.querySelector("#toggle").addEventListener("click", toggleSelect);

// test du this
console.log("à la racine du JS", this);
